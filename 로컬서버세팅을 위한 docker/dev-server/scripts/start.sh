#!/bin/bash
ln -s /etc/httpd /usr/local/apache
mkdir -p /usr/local/apache/bin/
ln -s /usr/sbin/rotatelogs /usr/local/apache/bin/rotatelogs
mkdir -p /usr/local/apache/conf/ssl
mkdir -p /usr/local/apache/htdocs
echo "^^ BraveCompany Local" > /usr/local/apache/htdocs/index.htm

mkdir -p /usr/local/php/lib/php/extensions/
ln -s /usr/lib64/php/modules /usr/local/php/lib/php/extensions/no-debug-non-zts-20151012

# sim
rm /data/project_test/bravelms/images && \
rm /data/project_test/bravelms/admin/bravecom && \
rm /data/project_test/bravelms/admin/module && \
rm /data/project_test/bravelms/academy/bravecom && \
rm /data/project_test/bravelms/academy/module && \
rm /data/project_test/bravelms/mobile/error && \
rm /data/project_test/bravelms/mobile/bravecom && \
rm /data/project_test/bravelms/mobile/module && \
rm /data/project_test/bravelms/admin/tzone/bravecom && \
rm /data/project_test/bravelms/admin/tzone/module && \
rm /data/project_test/bravelms/admin/admin_join/bravecom && \
rm /data/project_test/bravelms/admin/admin_join/module && \
rm /data/project_test/bravelms/academy/tzone/bravecom && \
rm /data/project_test/bravelms/academy/tzone/module && \
rm /data/project_test/bravelms/academy/admin_join/bravecom && \
rm /data/project_test/bravelms/academy/admin_join/module && \
rm /data/project_test/bravelms/bravecom/api/include/images && \
cd /data/project_test/bravelms && ln -s ../bravelms_img images && \
cd /data/project_test/bravelms/admin && ln -s ../bravecom bravecom && \
cd /data/project_test/bravelms/admin && ln -s ../module module && \
cd /data/project_test/bravelms/academy && ln -s ../bravecom bravecom && \
cd /data/project_test/bravelms/academy && ln -s ../module module && \
cd /data/project_test/bravelms/mobile && ln -s ../error error && \
cd /data/project_test/bravelms/mobile && ln -s ../bravecom bravecom && \
cd /data/project_test/bravelms/mobile && ln -s ../module module && \
cd /data/project_test/bravelms/admin/tzone && ln -s ../../bravecom bravecom && \
cd /data/project_test/bravelms/admin/tzone && ln -s ../../module module && \
cd /data/project_test/bravelms/admin/admin_join && ln -s ../../bravecom bravecom && \
cd /data/project_test/bravelms/admin/admin_join && ln -s ../../module module && \
cd /data/project_test/bravelms/academy/tzone && ln -s ../../bravecom bravecom && \
cd /data/project_test/bravelms/academy/tzone && ln -s ../../module module && \
cd /data/project_test/bravelms/academy/admin_join && ln -s ../../bravecom bravecom && \
cd /data/project_test/bravelms/academy/admin_join && ln -s ../../module module && \
cd /data/project_test/bravelms/bravecom/api/include && ln -s ../../../images images

# ssh
# openssl dhparam -out /usr/local/apache/conf/ssl/apache-selfsigned-dhparam.pem 2048
# openssl genrsa -out /usr/local/apache/conf/ssl/dev_local_server.key 2048
# openssl req -new -batch -out /usr/local/apache/conf/ssl/dev_local_server.csr -key /usr/local/apache/conf/ssl/dev_local_server.key -config /etc/local_ssl_openssl.cnf
# openssl x509 -req -days 3650 -in /usr/local/apache/conf/ssl/dev_local_server.csr -signkey /usr/local/apache/conf/ssl/dev_local_server.key -keyout /usr/local/apache/conf/ssl/apache-selfsigned.key -out /usr/local/apache/conf/ssl/apache-selfsigned.crt -extensions v3_req -extfile /etc/local_ssl_openssl.cnf
# openssl req -x509 -batch -nodes -days 365 -newkey rsa:2048 -keyout /usr/local/apache/conf/ssl/apache-selfsigned.key -out /usr/local/apache/conf/ssl/apache-selfsigned.crt -config /etc/local_ssl_openssl.cnf
# openssl genrsa -out /usr/local/apache/conf/ssl/apache-selfsigned.key 2048
# openssl req -new -batch -key /usr/local/apache/conf/ssl/apache-selfsigned.key -out /usr/local/apache/conf/ssl/apache-selfsigned.csr -config /etc/local_ssl_openssl.cnf
# openssl req -key /usr/local/apache/conf/ssl/apache-selfsigned.key -x509 -nodes -sha1 -days 365 -in /usr/local/apache/conf/ssl/apache-selfsigned.csr -out /usr/local/apache/conf/ssl/apache-selfsigned.crt

# openssl x509 -in /usr/local/apache/conf/ssl/apache-selfsigned.crt -noout -subject

openssl req -batch -x509 -nodes -days 3650 -newkey rsa:2048 -keyout /usr/local/apache/conf/ssl/apache-selfsigned.key -out /usr/local/apache/conf/ssl/apache-selfsigned.crt -config /etc/local_ssl_openssl.cnf
# openssl x509 -in /usr/local/apache/conf/ssl/apache-selfsigned.crt -text -noout | less

# SSHFS
mkdir -p /data/project_test/bravelms_file
echo rladlsgud | sshfs -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o reconnect bravecom@192.168.0.142:/data/project_test/bravelms_file /data/project_test/bravelms_file -o password_stdin
# ln -s /data/project_test/bravelms_file /data/project/bravelms_file

mkdir -p /data/project_test/bravelms_img
echo rladlsgud | sshfs -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o reconnect bravecom@192.168.0.142:/data/project_test/bravelms_img /data/project_test/bravelms_img -o password_stdin
# ln -s /data/project_test/bravelms_img /data/project/bravelms_img

# Start supervisord and services
exec /usr/bin/supervisord -n -c /etc/supervisord.conf
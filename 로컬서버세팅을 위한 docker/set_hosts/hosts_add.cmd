@ECHO OFF
call :isAdmin

if %errorlevel% == 0 (
goto :run
) else (
echo Requesting administrative privileges...
goto :UACPrompt
)

exit /b

:isAdmin
fsutil dirty query %systemdrive% >nul
exit /b

:run
ECHO INSTALL HOSTS
ECHO.>>"%WINDIR%\System32\drivers\etc\hosts"
COPY "%WINDIR%\System32\drivers\etc\hosts" + "%~dp0hosts_list.txt" "%WINDIR%\System32\drivers\etc\hosts"
ECHO.>>"%WINDIR%\System32\drivers\etc\hosts"
START notepad %WINDIR%\System32\drivers\etc\hosts
exit /b

:UACPrompt
echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"
echo UAC.ShellExecute "cmd.exe", "/c %~s0 %~1", "", "runas", 1 >> "%temp%\getadmin.vbs"

"%temp%\getadmin.vbs"
del "%temp%\getadmin.vbs"
exit /B
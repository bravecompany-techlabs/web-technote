# 용감한컴퍼니 GIT 개발 적용 문서

## 1. 구조 비교

### 1) 기존 구조

```mermaid
graph LR;
    id1["개발자/퍼블리셔"]--"① FTP로 원격파일 바로 수정"-->id2["테스트 웹서버<br>192.168.0.142"];
    id2--"② 기능 확인"-->id1;
    id2--"③ 기능테스트"-->id3["티켓발행자"];
    id3--"④ 수정사항 발견"-->id1;
    id3--"④ 이상무"-->id2;
    id2--"⑤ Intranet에서 RSync를 통한 소스파일 업데이트"-->id4[상용 웹서버1<br>192.168.0.139];
    id2--"⑤ Intranet에서 RSync를 통한 소스파일 업데이트"-->id5[상용 웹서버2<br>192.168.0.140];
    id2--"⑤ Intranet에서 RSync를 통한 리소스파일 업데이트"-->id6[상용 파일서버<br>192.168.0.145];
```

* 장점
  * FTP를 통한 수정으로 빠르게 작업 및 반영이 가능합니다.
  * FTP 접속만 되면되서 로컬에 별도의 특별한 세팅을 할 필요가 없습니다.

* 단점
  * 수정된 파일을 기억, 메세지, Jira 기록에 의존 해야합니다. (시간이 지나면 작업자 찾기 어려울 수 있습니다.)
  * FTP 파일을 원격에서 바로 수정하기 때문에, 같은 파일을 동시에 작업하기 어렵습니다.
  * 테스트 서버 한대에 여러 작업을 동시에 하면, 특정 소스코드 때문에 다른 사람이 영향을 받아서, 작업이 어려울 수 있습니다.

### 2) GIT 적용 구조

#### 간소화

```mermaid
graph LR;
    id1-1["개발자/퍼블리셔"]--"① 작업 및 기능 확인"-->id2-1["로컬 웹서버"];
    id1-1--"② 작업소스 PUSH"-->id3["GIT서버"];
    id3--"③ 소스 PULL"-->id4[테스트 웹서버];
    id4--"④ 기능 확인완료"-->id3;
    id3--"⑤ 소스 PULL"-->id5[실웹서버];
```

#### 상세

```mermaid
graph LR;
    id1-1["개발자/퍼블리셔"]--"② master브랜치로 부터<br>기능 브랜치 생성 후 작업"-->id2-1["로컬 웹서버<br>127.0.0.1"];
    id2-1--"③ 기능 확인"-->id1-1;
    id1-1--"④ 기능 브랜치를 develop브랜치에<br>병합 & PUSH"-->id3["GIT서버"];
    id3--"① master 브랜치 PULL"-->id1-1;
    id3--"⑤ develop 브랜치 PULL"-->id4["테스트 웹서버<br>192.168.0.142"];
    id4--"⑥ 다른 소스들과 합쳐진 후 기능 확인"-->id1-1;
    id1-1--"⑦ 기능테스트 의뢰"-->id5["티켓발행자"];
    id5--"⑧ 기능 확인"-->id4;
    id5--"⑨ 기능 확인 이상무"-->id1-1;
    id1-1--"⑩ 기능 브랜치를 master 브랜치에 병합 & PUSH"-->id3;
    id3--"⑪ master 브랜치 PULL"-->id6[실웹서버1<br>192.168.0.139];
    id3--"⑫ master 브랜치 PULL"-->id7[실웹서버2<br>192.168.0.140];
    id3--"⑬ master 브랜치 PULL"-->id8[실파일서버<br>192.168.0.145];
```

* 장점
  * GIT을 통해, 파일 이력관리가 됩니다. (누가 어떤 파일을 어떻게 수정했는지 다 기록 됩니다.)
  * 로컬 테스트 서버에서 작업하기 때문에, 수정사항에 대해서는 자기 자신만 영향을 받으며, 여러명이 같은 파일을 동시에 작업 할 수 있습니다.
  * 이력관리가 되기 때문에, 특정 시점으로 롤백하기 쉽습니다.
  * 브랜치를 나눠, 기능 단위별로 작업소스를 관리할 수 있습니다.

* 단점
  * 로컬에 별도의 개발환경 구축이 필요합니다.
  * 작업자 모두 GIT에 대한 이해와 스킬이 필요합니다.<br>특히 같은 부분이 수정될 경우 `충돌`이 발생할 수 있는데, 이 부분을 많이 어려워하는 것 같습니다.

## 2. Git 적용 전 정해야할 것

* GIT 서버
  * GIT 서버는 무엇으로 할지? GIT 솔루션 서버 설치 후 이용? GIT 호스팅 서비스 이용?<br>
  bitbucket.org 호스팅을 사용하는게 좋아보입니다.

* 브랜치 관리 전략
  * 브랜치는 어떻게 나누어서 관리를 할지? 참고:[우아한형제들 브랜치전략](http://woowabros.github.io/experience/2017/10/30/baemin-mobile-git-branch-strategy.html)<br>
  이전 회사에서는 master(실서버 브랜지), dev(테스트서버 브랜치), dev_작업자영어닉네임_기능명(로컬 기능브랜치) 으로 나눠서 작업했습니다.

* 로컬 테스트서버 세팅
  * VMWare, VirtualBox, Docker, Vagrant 등 무엇으로 할지?<br>

* 빌드 환경
  * 테스트 및 실서버에 코드 배포시(GIT PULL), 어떻게 할지? shell script로 구현 후 intranet에 UI 표현? Bitbucket의 CI/CD을 이용한 배포?

## 3. GIT 적용 방법

```mermaid
graph TD;
    id1["1. Git 서버 선택"]-->id2["2. 리포지토리 생성"];
    id2-->id3["3. 최신 소스코드 Git서버에 커밋 및 푸쉬"];
    id3-->id4["4. 테스트서버 Git 세팅"];
    id4-->id5["5. 각자 로컬개발 환경세팅"];
    id5-->id6["6. 실서버Git 세팅"];
```

### 1) GIT 서버 선택

* bitbucket.org 호스팅<br>
  유료/무료 가능 [Bitbucket Pricing](https://bitbucket.org/product/pricing), Jira Software integration 형태로 Jira 연동가능

* bitbucket.org 설치형<br>
  유료 [Bitbucket Self-Hosted Pricing](https://bitbucket.org/product/pricing?tab=self-hosted), Native Jira and Bamboo integrations 형태로 Jira 연동가능

* gitlab.com 호스팅<br>
  유료/무료 가능 [Gitlab Pricing](https://about.gitlab.com/pricing)

* gitlab.com 설치형<br>
  유료/무료 가능 [Gitlab Self-Managed Pricing](https://about.gitlab.com/pricing/#self-managed)

* github.com 호스팅<br>
  유료/무료 가능 [Github Pricing](https://github.com/pricing)

### 2) 리포지토리 생성

* [GIT서버 웹프로젝트](https://bitbucket.org/account/user/bravecomapnytf/projects/WEB) 에 각각 리포지토리 생성 (bravelms, bravelms_img, braveusr, iipa, unitelms)

### 3) 실서버 최신 소스코드 커밋

* master 브랜치 : 각 리포지토리에 소스코드 업로드(실서버 기준) 및 심볼릭 링크 재생성 후 커밋 (심볼릭링크 생성은 mac이나 리눅스에서 진행)

* develop 브랜치 : master 브랜치에서 브랜치를 딴 후 테스트서버 파일로 덮어씌운 후 커밋

### 4) 테스트서버 Git 세팅

* 테스트 서버 초기화 및 클론<br>
  (git 클론시 디렉토리가 비어있어야하므로, /data/project_git 디렉토리에 모든 것을 세팅 한 후 /data/project_git/.git -> project_test/.git로 디렉토리 복사를 진행해야 할 것으로 보입니다.)

* 최신 소스를 내려 받는 PULL(배포) 스크립트 생성

### 5) 각자 로컬 테스트 서버 세팅

#### a) Git 클라이언트(GUI) 설치

* [소스트리](https://www.sourcetreeapp.com): Atlassian의 것으로 Jira, Bitbucket 등 같은 회사에서 만든 것이므로 호환성이 좋을 것이라 생각됩니다. 버전 업데이트 되면서, Bitbucket 계정이 필수로 바뀐 것 같습니다.

* [Git크라켄](https://www.gitkraken.com): 소스트리를 대체할 만한 Git 클라이언트

* 위 두가지 외에도 많은 클라이언트가 있습니다. (VSCode를 사용하면, 위 2가지와 별도로 Gitlens 확장 모듈을 추가 설치를 추천합니다)

#### b) 로컬테스트 서버를 위한 가상머신(Virtual Machine) 설치 및 서버 세팅

* VMWare: 가상머신 생성 후 아래의 사양정보를 보고 세팅

* VirtualBox: 가상머신 생성 후 아래의 사양정보를 보고 세팅

* Vagrant: 자동화 세팅

* Docker(Hyper-V or VirtualBox): Docker compose를 통해서, 자동화 세팅

#### c) 서버 사양 정보

* 서버 사양 정보
  * 실웹서버 139: CentOS Linux release 7.4.1708 (Core), PHP 7.0.4 NTS, Apache/2.4.10
  * 실웹서버 140: CentOS Linux release 7.6.1810 (Core), PHP 7.0.4 NTS, Apache/2.4.10
  * 테스트웹서버 142: CentOS Linux release 7.4.1708 (Core), PHP 7.0.4 NTS, Apache/2.4.10

<details markdown="1">
<summary>자세히</summary>

```bash
# 아파치 버전 확인
/usr/local/apache/bin/apachectl -v

# PHP 버전 확인
/usr/local/php/bin/php -v
```

* 로드된 아파치 모듈
  * core_module (static)
  * so_module (static)
  * http_module (static)
  * mpm_prefork_module (static)
  * authn_file_module (shared)
  * authn_dbm_module (shared)
  * authn_anon_module (shared)
  * authn_dbd_module (shared)
  * authn_socache_module (shared)
  * authn_core_module (shared)
  * authz_host_module (shared)
  * authz_groupfile_module (shared)
  * authz_user_module (shared)
  * authz_dbm_module (shared)
  * authz_owner_module (shared)
  * authz_dbd_module (shared)
  * authz_core_module (shared)
  * access_compat_module (shared)
  * auth_basic_module (shared)
  * auth_form_module (shared)
  * auth_digest_module (shared)
  * allowmethods_module (shared)
  * file_cache_module (shared)
  * cache_module (shared)
  * cache_disk_module (shared)
  * cache_socache_module (shared)
  * socache_shmcb_module (shared)
  * socache_dbm_module (shared)
  * socache_memcache_module (shared)
  * macro_module (shared)
  * dbd_module (shared)
  * dumpio_module (shared)
  * buffer_module (shared)
  * ratelimit_module (shared)
  * reqtimeout_module (shared)
  * ext_filter_module (shared)
  * request_module (shared)
  * include_module (shared)
  * filter_module (shared)
  * substitute_module (shared)
  * sed_module (shared)
  * deflate_module (shared)
  * mime_module (shared)
  * log_config_module (shared)
  * log_debug_module (shared)
  * logio_module (shared)
  * env_module (shared)
  * expires_module (shared)
  * headers_module (shared)
  * unique_id_module (shared)
  * setenvif_module (shared)
  * version_module (shared)
  * remoteip_module (shared)
  * proxy_module (shared)
  * proxy_connect_module (shared)
  * proxy_ftp_module (shared)
  * proxy_http_module (shared)
  * proxy_fcgi_module (shared)
  * proxy_scgi_module (shared)
  * proxy_wstunnel_module (shared)
  * proxy_ajp_module (shared)
  * proxy_balancer_module (shared)
  * proxy_express_module (shared)
  * session_module (shared)
  * session_cookie_module (shared)
  * session_dbd_module (shared)
  * slotmem_shm_module (shared)
  * ssl_module (shared)
  * lbmethod_byrequests_module (shared)
  * lbmethod_bytraffic_module (shared)
  * lbmethod_bybusyness_module (shared)
  * lbmethod_heartbeat_module (shared)
  * unixd_module (shared)
  * dav_module (shared)
  * status_module (shared)
  * autoindex_module (shared)
  * info_module (shared)
  * cgi_module (shared)
  * dav_fs_module (shared)
  * vhost_alias_module (shared)
  * negotiation_module (shared)
  * dir_module (shared)
  * actions_module (shared)
  * speling_module (shared)
  * userdir_module (shared)
  * alias_module (shared)
  * rewrite_module (shared)
  * php7_module (shared)

```bash
# 로드된 아파치 모듈 확인
/usr/local/apache/bin/apachectl -M
```

* 로드된 PHP 모듈
  * Core
  * ctype
  * curl
  * date
  * dom
  * exif
  * fileinfo
  * filter
  * ftp
  * gd
  * hash
  * iconv
  * json
  * libxml
  * mbstring
  * mcrypt
  * mysqli
  * mysqlnd
  * okname
  * openssl
  * pcre
  * PDO
  * pdo_mysql
  * pdo_sqlite
  * Phar
  * posix
  * Reflection
  * session
  * SimpleXML
  * soap
  * sockets
  * SPL
  * sqlite3
  * standard
  * sysvshm
  * tokenizer
  * xml
  * xmlreader
  * xmlwriter
  * zip
  * zlib

```bash
# 로드된 PHP모듈 확인
/usr/local/php/bin/php -m
```

</details>

#### d) 로컬서버 파일 동기화를 위한 에디터 또는 프로그램 세팅

* 로컬컴퓨터와 로컬 테스트서버(VM)간의 파일 동기화를 위해 FTP 세팅
  VSCode는 SFTP 추천

* FreeFileSync와 같은 프로그램을 써도 됩니다.

* Docker로 세팅 했을 경우, 소스 폴더를 볼륨으로 지정해놓으면 별도의 FTP로 파일 동기화 작업은 안해도 될 것 같습니다.

### 6) 실서버 Git 세팅

* 실서버 초기화 및 클론<br>
  (git 클론시 디렉토리가 비어있어야하므로, /data/project_git 디렉토리에 모든 것을 세팅 한 후 /data/project_git/.git -> project/.git로 디렉토리 복사를 진행해야 할 것으로 보입니다.)

* 최신 소스를 내려 받는 PULL(배포) 스크립트 생성

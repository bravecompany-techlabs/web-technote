# 용감한컴퍼니 버티컬(bravelms) 파일 명세서

## /data/project/bravelms (정리중...)
    admin/                                  -- 플랫폼: 관리자 페이지
    mobile/                                 -- 플랫폼: 모바일웹


    board/                                  -- 페이지: 게시판
        spot/                               -- 
            spot/                           -- 
                event_tel_advice.php        -- 
                memoirs.php                 -- 
                sulmun.php                  -- 
                tel_advice.php              -- 
                vita_event.php              -- 
            event_tel_advice.php            -- 
            memoirs.php                     -- 
            sulmun.php                      -- 
            tel_advice.php                  -- 
            vita_event.php                  -- 
        board.php                           -- 페이지: 카톡상담후기
        free_file.php                       -- 페이지: 무료자료실
        index.php                           -- 페이지: 게시판 (사용 예 board/schedule)
        std_eval.php                        -- 페이지: 수강후기
        std_exp.php                         -- 페이지: 수능성적폭발후기
        std_level.php                       -- 페이지: 오픽 레벨테스트 (용감한스피킹)
        std_oneday.php                      -- 페이지: 하루 행정학 한문제 (스마트행정학)
        std_pds.php                         -- 페이지: 학습자료실
        std_qna.php                         -- 페이지: 학습문의(용감한스피킹)
        std_review.php                      -- 페이지: 리뷰게시판
        today_std_eval.php                  -- 페이지: 오늘의 학습후기
    book/                                   -- 페이지: 교재
        bkReview.php                        -- 페이지: 교재 리뷰(리라클영어)
        book.php                            -- 페이지: 교재 상세(리라클영어, 모두자)
        book_dtl_ajax.php                   -- 페이지: 교재 상세 팝업
        book_file.php                       -- 페이지: 교재자료실(리라클영어, 모두자)
        index.php                           -- 페이지: 교재 메인
    bravecom/                               -- PHP 프로그램 로직
        api/                                -- 
            include/                        -- 
                mail/                       -- 
                    140421.htm              -- 
                    birthday.htm            -- 
                    join.htm                -- 
                    pay.htm                 -- 
                    search_pw.htm           -- 
                    withdraw.htm            -- 
                AdminManager.php            -- 
                BoardManager.php            -- 
                BongSpaManager.php          -- 
                CodeManager.php             -- 
                GoodsManager.php            -- 
                Manager.inc.php             -- 
                Mysql_DB.php                -- 
                PayManager.php              -- 
                Rest.inc.php                -- 
                SmtpCls.php                 -- 
                TestManager.php             -- 
                UserManager.php             -- 
                VideoManager.php            -- 
            api.php                         -- 
            index.php                       -- 
        batchjob/                           -- 
            batchjob/                       -- 
                adm_authkey.php             -- 
                bskt_cpn.php                -- 
                bskt.php                    -- 
                cross_drvoca_dhjbasic.php   -- 
                cross_job_test.php          -- 
                cross_step.php              -- 
                daily_stat2222.php          -- 
                daily_stat.php              -- 
                drvoca_GDN_evt.php          -- 
                event_drvoca_20141231.php   -- 
                evt_150922.php              -- 
                evt_dhjcollabo_151013.php   -- 
                evt_dhjeng2.php             -- 
                evt_dhjeng.php              -- 
                evt_drvoca_151201.php       -- 
                evt_drvoca2.php             -- 
                evt_drvoca_5.php            -- 
                evt_drvoca.php              -- 
                evt.php                     -- 
                friend_bravespk.php         -- 
                friend_dhjbasic.php         -- 
                lms_bravespk.php            -- 
                lms_ygmath.php              -- 
                rst_evt.txt                 -- 
                rst.txt                     -- 
                stdchr_mng.php              -- 
                std_qna.php                 -- 
                stl_month_stat.php          -- 
                stl_state.php               -- 
                test.php                    -- 
                test.txt                    -- 
                usr_dormancy_send2.php      -- 
                usr_dormancy_send.php       -- 
                vbank.php                   -- 
            adm_authkey.php                 -- 
            bskt_cpn.php                    -- 
            bskt.php                        -- 
            bskt_sms.php                    -- 
            cpn_notice.php                  -- 
            cross_drvoca_dhjbasic.php       -- 
            cross_job_test.php              -- 
            cross_step.php                  -- 
            daily_stat2222.php              -- 
            daily_stat.php                  -- 
            drvoca_GDN_evt.php              -- 
            event_drvoca_20141231.php       -- 
            evt_150922.php                  -- 
            evt_bravespk_leveltest.php      -- 
            evt_dhjcollabo_151013.php       -- 
            evt_dhjeng2.php                 -- 
            evt_dhjeng.php                  -- 
            evt_drvoca_151201.php           -- 
            evt_drvoca2.php                 -- 
            evt_drvoca_5.php                -- 
            evt_drvoca.php                  -- 
            evt_mdcop_hyoja.php             -- 
            evt_mdgong_hyeja.php            -- 
            evt.php                         -- 
            finance_stl.php                 -- 
            friend_bravespk.php             -- 
            friend_dhjbasic.php             -- 
            inicis_batch.php                -- 
            in_sacinfo.php                  -- 
            lms_bravespk.php                -- 
            lms_ygmath.php                  -- 
            rst_evt.txt                     -- 
            rst.txt                         -- 
            search_txt1.txt                 -- 
            std_alarm_sms.php               -- 
            stdchr_mng.php                  -- 
            std_qna.php                     -- 
            stl_month_stat.php              -- 
            stl_state.php                   -- 
            TenPing.php                     -- 
            test.php                        -- 
            test.txt                        -- 
            usr_dormancy_send2.php          -- 
            usr_dormancy_send.php           -- 
            vbank.php                       -- 
        board/                              -- 
            board.php                       -- 
            free_file.php                   -- 
            index.php                       -- 
            spot                            -- 
            std_eval.php                    -- 
            std_exp.php                     -- 
            std_level.php                   -- 
            std_oneday.php                  -- 
            std_pds.php                     -- 
            std_qna.php                     -- 
            std_review.php                  -- 
            today_std_eval.php              -- 
        book/                               -- 
            bkReview.php                    -- 
            book_dtl_ajax.php               -- 
            book_file.php                   -- 
            book.php                        -- 
            index.php                       -- 
        common/                             -- 
            basic_cls.php                   -- 
            basic_fn.php                    -- 
            CookieChk_cls.php               -- 
            FileLog_cls.php                 -- 
            interface_cls.php               -- 
            JSON.php                        -- 
            MySqlDB_cls.php                 -- 
            MySqliDB_cls.php                -- 
            pgCalcul_cls.php                -- 
            prod_cls.php                    -- 
            setPage_cls.php                 -- 
            Smtp_cls.php                    -- 
        company/                            -- 
            index.php                       -- 
        cs/                                 -- 
            event.php                       -- 
            faq.php                         -- 
            notice.php                      -- 
            program.php                     -- 
            qna.php                         -- 
            refund.php                      -- 
            std_faq_ajax.php                -- 
            std_faq.php                     -- 
        css/                                -- 
            common.php                      -- 
            main.php                        -- 
            reset.php                       -- 
        event/                              -- 
            event.php                       -- 
            exec.php                        -- 
            index.php                       -- 
        evtpop/                             -- 
            popup.php                       -- 
        fvod/                               -- 
            chr_cmt.php                     -- 
            chr_list.php                    -- 
            chr_view.php                    -- 
            explain_info.php                -- 
            free_chr.php                    -- 
            fvod_cnt.php                    -- 
            fvod_rpy.php                    -- 
            lct_list.php                    -- 
            tcc_list.php                    -- 
            tcc_view.php                    -- 
        inc/                                -- 
            adm_code_define.php             -- 
            code_define.php                 -- 
            config_inc.php                  -- 
            footer.php                      -- 
            header.php                      -- 
            pop_footer.php                  -- 
            pop_header.php                  -- 
            style.php                       -- 
        js/                                 -- 
            admin.js                        -- 
            appable.js                      -- 
            common.js                       -- 
            highcharts-custom.js            -- 
            highcharts-custom.min.js        -- 
            jquery.flipster.infiniteloop.js -- 
            jquery.js                       -- 
            jquery.monthpicker.js           -- 
            jquery-sortable.js              -- 
            jquery.tubular.1.0.js           -- 
        lab/                                -- 
            guideline_ajax.php              -- 
            guideline.php                   -- 
            std_analy.php                   -- 
            std_info.php                    -- 
        lib/                                -- 
            bskt_cpn_pop.php                -- 
            filedown.php                    -- 
            fvod_his.php                    -- 
            manage_event_ajax.php           -- 
            mk_bskt_cpn.php                 -- 
            mk_cpn_ajax.php                 -- 
            msg_usr_typ.php                 -- 
            page_list.php                   -- 
            scrap.php                       -- 
            stay_tm.php                     -- 
        member/                             -- 
            email_auth_ajax.php             -- 
            email_auth.php                  -- 
            id_check_ajax.php               -- 
            idnpw_search_ajax.php           -- 
            id_search.php                   -- 
            info_search.php                 -- 
            join_action.php                 -- 
            join_band_action.php            -- 
            join_band.php                   -- 
            join.php                        -- 
            join_poll_ajax.php              -- 
            join_poll_ajax_test.php         -- 
            join_step1.php                  -- 
            join_step2.php                  -- 
            join_step3.php                  -- 
            join_stp2.php                   -- 
            join_stp.php                    -- 
            login_action_jsonp.php          -- 
            login_action.php                -- 
            login_maction2.php              -- 
            login_maction.php               -- 
            login.php                       -- 
            login_rst.php                   -- 
            login_sns.php                   -- 
            login_sns_rst.php               -- 
            logout.php                      -- 
            mdf_info.php                    -- 
            mdf_out.php                     -- 
            mdf_pwd_change.php              -- 
            mdf_uinfo_action.php            -- 
            pw_search.php                   -- 
            sso_ajax.php                    -- 
            sso.php                         -- 
            uinfo_transfer.php              -- 
        mypage/                             -- 
            bank_ajax.php                   -- 
            bank.php                        -- 
            basket.php                      -- 
            board_level.php                 -- 
            board.php                       -- 
            board_qna.php                   -- 
            index.php                       -- 
            message.php                     -- 
            message_save.php                -- 
            pay_ajax.php                    -- 
            pay_list.php                    -- 
            pay_view.php                    -- 
            scrap.php                       -- 
            std_add_view.php                -- 
            std_dtl_ajax.php                -- 
            std_list_ajax_1.php             -- 
            std_list_ajax.php               -- 
            std_list.php                    -- 
            std_view.php                    -- 
        payment/                            -- 
            callback.php                    -- 
            cas_callback.php                -- 
            friend_chk_ajax.php             -- 
            index.php                       -- 
            kicc_callback.php               -- 
            kicc_cas_callback.php           -- 
            kicc_ordering.php               -- 
            order_exec.php                  -- 
            ordering.php                    -- 
            result.php                      -- 
            sac_exec.php                    -- 
            stl_exec.php                    -- 
            stl_sms.php                     -- 
        player/                             -- 
            callback_drm.php                -- 
            callback_info.php               -- 
            callback_lms.php                -- 
            callback_play.php               -- 
            expn_player.php                 -- 
            flash_player.php                -- 
            info.php                        -- 
            player_action2.php              -- 
            player_action.php               -- 
            player.php                      -- 
        popup/                              -- 
            exam/                           -- 
            svr/                            -- 
            book_preview.php                -- 
            cash_charge.php                 -- 
            cpn_reg2.php                    -- 
            cpn_reg.php                     -- 
            cpn_use.php                     -- 
            device_init.php                 -- 
            file_preview.php                -- 
            personal_policy.php             -- 
            point_charge.php                -- 
            pop_info.php                    -- 
            pop_print.php                   -- 
            popup.php                       -- 
            popup_relax_my.php              -- 
            pop_view.php                    -- 
            search_address.php              -- 
            selPkg.php                      -- 
            service_policy.php              -- 
            std_ext.php                     -- 
            std_noti.php                    -- 
            std_proof.php                   -- 
            std_stp.php                     -- 
        private/                            -- 
            private_info.php                -- 
            private_noti.php                -- 
        prod/                               -- 
            chr_info.php                    -- 
            chr_list.php                    -- 
            chr_view.php                    -- 
            pass_view.php                   -- 
            pkg_view.php                    -- 
            prod_ajax.php                   -- 
        promotion/                          -- 
            index.php                       -- 
        sh/                                 -- 쉘스크립트
            rsync_cp.sh                     -- /data/project_test/bravelms_img를 211.233.58.61로 동기화 하는데 211.233.58.61가 어딘지 모르겠음
            rsync_file.sh                   -- 파일업로드 rsync로 192.168.0.145에 동기화
            rsync_img.sh                    -- 이미지 업로드 rsync로 192.168.0.145에 동기화
            rsync_imsi_make.sh              -- 211.233.58.61로 동기화 하는데 211.233.58.61가 어딘지 모르겠음
            rsync_real_make.sh              -- 211.233.58.61로 동기화 하는데 211.233.58.61가 어딘지 모르겠음
            rsync_source.sh                 -- 211.233.58.61로 동기화 하는데 211.233.58.61가 어딘지 모르겠음
            rsync_test_make.sh              -- 211.233.58.61로 동기화 하는데 211.233.58.61가 어딘지 모르겠음
        tch/                                --
            chr_intro.php                   -- 
            dhj_intro.php                   -- 
            site_info.php                   -- 
            tch_info.php                    -- 
        DataList.php                        --
        jquery.css                          --
        main.php                            --
        robots.txt                          --
    company/                                -- 페이지: 사이트소개
        index.php                           -- 페이지: 사이트소개(안쓰는 페이지??)
    cs/                                     -- 페이지: 고객센터
        event.php                           -- 페이지: 이벤트(안쓰는 페이지??)
        faq.php                             -- 페이지: 고객센터 - 자주하는질문
        notice.php                          -- 페이지: 고객센터 - 공지사항
        program.php                         -- 페이지: 고객센터 - 학습프로그램
        qna.php                             -- 페이지: 고객센터 - 1:1문의
        refund.php                          -- 페이지: 환불조회
        std_faq.php                         -- 페이지: ?? 안쓰는 페이지?
        std_faq_ajax.php                    -- Ajax: std_faq 데이터 받는 페이지 같음
    error/                                  -- 에러 페이지
        dbError.htm                         -- 페이지: DB에러
        expPage.htm                         -- 페이지: 만료
        inspect.htm                         -- 페이지: 서비스 점검
        notPage.htm                         -- 페이지: 404
        bg_SiteError_area.png               -- 이미지: 에러페이지 배경이미지로 사용
        logo.png                            -- 이미지: 용감한 컴퍼니 로고(error 페이지에서 사용)
        error.css                           -- CSS: 용감한 컴퍼니 에러 페이지 CSS
    evtpop/                                 -- 이벤트 팝업
        popup.php                           -- 페이지: 이벤트팝업으로 추정(근데 현재는 사용하지 않는 것으로 보임)
    fvod/                                   -- 무료강좌
        chr_cmt.php                         -- 페이지: 해설강의
        chr_list.php                        -- 페이지: 커리큘럼 목록
        chr_view.php                        -- 페이지: 강의 뷰 같은데 skins/common/prod/chr_view.htm 라는 파일이 없음
        explain_info.php                    -- 페이지: 해설강의
        free_chr.php                        -- 페이지: 무료강의 - 커리큘럼
        fvod_cnt.php                        -- Ajax: 무료강의 조회 로그 등록
        fvod_rpy.php                        -- 페이지: 무료강의 - 댓글 뷰
        lct_list.php                        -- 페이지: 강의 리스트
        tcc_list.php                        -- 페이지: 무료강의 - 가로세로캐스트
        tcc_view.php                        -- 페이지: 무료강의 - 가로세로캐스트 뷰
    mobile/                                 -- mobile.도메인 페이지
    images/                                 -- 이미지 경로 심볼릭링크 (../bravelms_img)
    lab/                                    -- 페이지: 연구소
        guideline_ajax.php                  -- 페이지: 연구소 소개
        guideline.php                       -- 페이지: 연구소 소개
        std_analy.php                       -- 페이지: 수능영어칼분석(덩허접수능영어)(수능시행개요, 수능영어출제경향, 절대평가개요)
        std_info.php                        -- 페이지: 수능영어칼분석(덩허접수능영어)(EBS&적중 믿지마라, 선해석 후독해)
    member/                                 -- 페이지: 회원가입
        email_auth_ajax.php                 -- Ajax: 이메일 인증 안내 메일 발송 (응답형식 JSON)
        email_auth.php                      -- 페이지: 이메일 인증 처리 페이지(메일 발송되는 링크 /member/email_auth.php?auth_key=<<VAR:AUTH_KEY>>&email=<<VAR:USR_MAIL>>&ret_url=<<VAR:RET_URL>>)
        id_check_ajax.php                   -- Ajax: 사용가능한 아이디 인지 체크 (응답형식:문자열메세지)
        idnpw_search_ajax.php               -- AJax: 아이디 비밀번호 찾기 (응답형식 JSON)
        id_search.php                       -- 페이지: 아이디 찾기
        info_search.php                     -- 페이지: 아이디/비밀번호 찾기 인트로
        join_action.php                     -- 데이터처리: 회원가입처리
        join_band_action.php                -- 데이터처리: 제휴회원가입처리(join_band)
        join_band.php                       -- 페이지: 제휴회원가입
        join.php                            -- 페이지: 회원가입
        join_poll_ajax.php                  -- Ajax: 회원가입 설문 처리 (응답형식 JSON)
        join_step1.php                      -- 페이지: 회원가입 약관 동의 및 이름, 핸드폰 인증
        join_step2.php                      -- 페이지: 회원가입 정보입력
        join_step3.php                      -- 페이지: 회원가입완료(안쓰는 것 같음)
        join_stp2.php                       -- 페이지: 회원가입(옛날 것으로 추정, 20190905까지도 쓰고 있음) 완료 페이지
        join_stp.php                        -- 페이지: 회원가입(옛날 것으로 추정) 스탭1
        login_action_jsonp.php              -- 페이지: 닥터보카 0원패키지 로그인처리(https://www.drvoca.com/event/?evt_no=21)
        login_action.php                    -- Ajax: 로그인 처리 (응답형식:문자열코드)
                                            -- [S0:가입대기, S1:휴먼회원, S2:정상로그인, S3:정지회원 그외 아이디 비번 일치하지 않음]
        login.php                           -- 페이지: 로그인
        login_rst.php                       -- 페이지: 로그인 결과
        login_sns.php                       -- 페이지: 소셜로그인
        login_sns_rst.php                   -- 페이지: 소셜로그인 결과
        logout.php                          -- 페이지: 로그아웃
        mdf_info.php                        -- 페이지: 회원정보관리/비밀번호관리/회원탈퇴
        mdf_out.php                         -- 데이터처리: 회원탈퇴
        mdf_pwd_change.php                  -- 데이터처리: 패스워드 변경
        mdf_uinfo_action.php                -- 데이터처리: 회원정보 변경
        pw_search.php                       -- 페이지: 비밀번호 찾기
        search_address.php                  -- 페이지: 주소검색
        sso_ajax.php                        -- Ajax: One Pass ID로 전환(응답형식 JSON)
        sso_child.php                       -- 데이터처리: 통합에서 각 사이트로 세션 동기화를 위한 페이지(https://member.bravecompany.net/php/sso.php?site_seq=11&loc=join에서 https://www.dhjengsn.com/member/sso_child.php?ukey=1909041029073708&loginkeep=&loc=join&key=11로 호출)
        sso.php                             -- 페이지: One Pass ID로 동의 페이지
        tmp.php                             -- 페이지: 안쓰는 페이지 같음, 임시로 생성한 페이지
        uinfo_transfer.php                  -- Ajax: 회원전환 처리 페이지
    module/                                 -- 모듈 폴더
        Auth_Okname/                        -- 본인인증모듈: OKNAME(KCB)
        datepicker/                         -- JS라이브러리: helloCalender
        FaceBook/                           -- PHP라이브러리: Facebook SDK
        Highcharts/                         -- JS라이브러리: Highcharts
        jquery/                             -- JS라이브러리: jQuery UI
        jqwidgets/                          -- JS라이브러리: jqwidgets
        Kollus/                             -- Kollus Player
        Mobile-Detect-master/               -- PHP라이브러리: Mobile_Detect (https://github.com/serbanghita/Mobile-Detect)
        NaverSyndication/                   -- 네이버 신디케이션
        Pay/                                -- 결제모듈
            IniPay/                         -- 결제모듈: 이니시스
            IniPay-mobile/                  -- 결제모듈: 이니시스(모바일)
            Kicc/                           -- 결제모듈: KICC 이지페이
            Kicc-mobile/                    -- 결제모듈: KICC 이지페이(모바일)
            lgU-SmartXPay/                  -- 결제모듈: LG XPay(모바일)
            lgU-XPay/                       -- 결제모듈: LG XPay
        PHPExcel/                           -- PHP라이브러리: Excel Open/Save 라이브러리
        robots.txt                          -- 검색봇 룰 정의
        SmartEditor/                        -- JS라이브러리: 네이버스마트에디터 2.0
        sms/                                -- SMS 문자 발송 모듈
            cafe24.php                      -- Cafe24 SMS 모듈 (아래꺼 말고 이거 쓰는 것 같음)
            sms/                            -- SMS 문자 발송 모듈
                cafe24.php                  -- Cafe24 SMS 모듈
        snsAPI/                             -- Auth 인증 라이브러리
    mypage/                                 -- 페이지: 마이페이지
        bank_ajax.php                       -- 
        bank.php                            -- 페이지: 마이페이지 - 캐시/마일리지/쿠폰/체험권
        basket.php                          -- 페이지: 마이페이지 - 장바구니
        board_level.php                     -- 
        board.php                           -- 페이지: 마이페이지 - 학습Q&A 목록
        board_qna.php                       -- 
        index.php                           -- 
        message.php                         -- 페이지: 받은 쪽지함
        message_save.php                    -- 페이지: 쪽지 보관함
        pay_ajax.php                        -- 
        pay_list.php                        -- 페이지: 주문/배송조회 목록
        pay_view.php                        -- 페이지: 주문/배송조회 뷰
        scrap.php                           -- 
        std_add_view.php                    -- 
        std_dtl_ajax.php                    -- Ajax: 회독 관련 데이터 처리
        std_list_ajax_1.php                 -- Ajax: 수강시작, 수강리스트가 있는데 안쓰는 파일로 추정
        std_list_ajax.php                   -- Ajax: 수강정지, 모바일기기초기화, 수강증, 나의게시판, 수강시작 종료일 변경, 수강리스트 등등 std_list.php에 대한 데이터처리 (응답형식 JSON)
        std_list.php                        -- 페이지: 수강중인강좌/수강종료강좌
        std_view.php                        -- 페이지: 수강중인강좌/수강종료강좌 뷰
    payment/                                -- 페이지: 결제
        callback.php                        -- 
        cas_callback.php                    -- 
        friend_chk_ajax.php                 -- 
        index.php                           -- 
        kicc_callback.php                   -- 
        kicc_cas_callback.php               -- 
        kicc_ordering.php                   -- 
        order_exec.php                      -- 
        ordering.php                        -- 
        result.php                          -- 
        sac_exec.php                        -- 
        stl_exec.php                        -- 
        stl_sms.php                         -- 
    player/                                 -- 페이지: 
        callback_drm.php                    -- 
        callback_info.php                   -- 
        callback_lms.php                    -- 
        callback_play.php                   -- 
        expn_player.php                     -- 
        flash_player.php                    -- 
        info.php                            -- 
        player_action2.php                  -- 
        player_action.php                   -- 
        player.php                          -- 
    popup/                                  -- 페이지: 팝업
        exam/                               -- 
            exam.php                        -- 페이지: 시험
        svr/                                -- 
            svr.php                         -- 페이지: 설문조사
        book_preview.php                    -- 페이지: 교재 미리보기(PDF 파일 미리보기)
        cash_charge.php                     -- 페이지: 캐시충전
        cpn_reg2.php                        -- 페이지: 체험권 등록
        cpn_reg.php                         -- 페이지: 쿠폰 등록
        cpn_use.php                         -- 페이지: 쿠폰 사용
        device_init.php                     -- 페이지: 모바일기기초기화 신청하기
        file_preview.php                    -- 페이지: PDF 파일 미리보기
        personal_policy.php                 -- 페이지: 개인정보취급방침
        point_charge.php                    -- 페이지: 마일리지 충전(쿠폰 등록)
        pop_info.php                        -- 페이지: 팝업 목록 JSON ()
        pop_print.php                       -- 
        popup.php                           -- 페이지: 팝업 경로로 열기 (/popup/popup.php?path=/mock/mock_enter.htm)
        popup_relax_my.php                  -- 페이지: 계정 휴면상태 알림 팝업
        pop_view.php                        -- 
        search_address.php                  -- 페이지: 주소검색
        selPkg.php                          -- 
        service_policy.php                  -- 페이지: 이용약관
        std_ext.php                         -- 페이지: 수강연장하기
        std_noti.php                        -- 페이지: 학습 안내사항
        std_proof.php                       -- 페이지: 수강증발급
        std_stp.php                         -- 페이지: 일시정지하기
    private/                                -- 
        private_info.php                    -- 
        private_noti.php                    -- 
    prod/                                   -- 페이지: 상품
        chr_info.php                        -- 
        chr_list.php                        -- 
        chr_view.php                        -- 
        pass_view.php                       -- 
        pkg_view.php                        -- 
        prod_ajax.php                       -- 
    promotion/                              -- 페이지: 프로모션
        index.php                           -- 
    skins/                                  -- 각 사이트별 뷰 파일, 리소스 파일이 있는 폴더
        _common_skins/                      -- 공통스킨
            prod/                           -- 
                book_view_pop.htm           -- 
                chr_list.htm                -- 
                chr_view.htm                -- 
                chr_view_pop.htm            -- 
                eval_view_pop.htm           -- 
                pass_view.htm               -- 
            common_fn.php                   -- 
            common_left.php                 -- 
            crm_auto_pop.php                -- 
            filePreview.htm                 -- 
            first_inactive_pop.php          -- 
            main_bskt_pop.php               -- 
            main_layer_pop1.php             -- 
            main_layer_pop2.php             -- 
            mopyeong_pop.php                -- 
            mypage_bskt_pop.php             -- 
            newyear_evt_pop.php             -- 
            pass_confirm_pop.php            -- 
            sale_end_pop.php                -- 
            sel_book.php                    -- 
            service_end_pop.php             -- 
            test_review_1804.php            -- 
            test_review_gong.php            -- 
        {스킨이름폴더}/                      -- 각 사이트별로 존재
            board/                          -- 수능 성적폭발후기, 수강후기 등 게시판
                board.htm                   -- 페이지: 카톡상담후기
                board_view.htm              -- 페이지: 카톡상담후기 뷰
                free_file_list.htm          -- 페이지: 무료자료실 목록
                free_file_view.htm          -- 페이지: 무료자료실 뷰
                guide.htm                   -- 페이지: 가이드
                info_file_list.htm          -- 페이지: 입시사자료실 목록 // https://www.cosmosbio.kr/bridge.php?path=/board/info_file_list.htm
                info_file_view.htm          -- 페이지: 입시자료실 뷰     // https://www.cosmosbio.kr/bridge.php?path=/board/info_file_list.htm
                laboratory.htm              -- 페이지: (안쓰는 파일 같음)
                list.htm                    -- 페이지: (안쓰는 파일 같음)
                schedule.htm                -- 페이지: 시험일정 게시판
                std_eval_best.htm           -- 페이지: 성공체험기 목록 (안쓰는 파일 같음)
                std_eval_list.htm           -- 페이지: 수강후기 목록
                std_eval_view.htm           -- 페이지: 수강후기 뷰
                std_eval_write.htm          -- 페이지: 수강후기 쓰기
                std_exp_list.htm            -- 페이지: 수능성적폭발후기 목록
                std_exp_view.htm            -- 페이지: 수능성적폭발후기 뷰
                std_exp_write.htm           -- 페이지: 수능성적폭발후기 쓰기
                std_pds_list.htm            -- 페이지: 학습자료실 목록
                std_pds_view.htm            -- 페이지: 학습자료실 뷰
                std_qna_adminview.htm       -- 페이지: 학습문의 답변보기
                std_qna_list.htm            -- 페이지: 학습문의 목록
                std_qna_opic.htm            -- 페이지: 학습문의(용감한스피킹) 오픽
                std_qna_tos.htm             -- 페이지: 학습문의(옹감한스피킹) 토익스피킹
                std_qna_view.htm            -- 페이지: 학습문의
                std_qna_write.htm           -- 페이지: 학습문의
                std_review_list.htm         -- 페이지: 리뷰게시판 목록
                std_review_view.htm         -- 페이지: 리뷰게시판 뷰
                today_std_eval_list.htm     -- 페이지: 오늘의 학습후기
            book/                           -- 페이지: 교재
                bkReview_list.htm           -- 페이지: 교재후기 목록
                bkReview_view.htm           -- 페이지: 교재후기 뷰
                bkReview_write.htm          -- 페이지: 교재후기 쓰기
                book_dtl_ajax.htm           -- 페이지: 교재 상세 팝업
                book_file_list.htm          -- 페이지: 교재자료실 목록
                book_file_view.htm          -- 페이지: 교재라료실 뷰
                book.htm                    -- 페이지: 교재 상세
                index.htm                   -- 페이지: 교재 목록
            company/                        -- 페이지: 사이트(회사)소개
                index.htm                   -- 페이지: 사이트(회사)소개
            cs/                             -- 페이지: 고객센터
                event.htm                   -- 페이지: 이벤트(지금 안쓰는 이벤트 게시판 같음)
                faq.htm                     -- 페이지: 고객센터 - 자주하는질문 목록
                faq_view.htm                -- 페이지: 고객센터 - 자주하는질문 뷰
                notice.htm                  -- 페이지: 고객센터 - 공지사항 목록
                notice_view.htm             -- 페이지: 고객센터 - 공지사항 뷰
                program.htm                 -- 페이지: 고객센터 - 학습프로그램
                qna_edit.htm                -- 페이지: 고객센터 - 1:1문의 수정
                qna.htm                     -- 페이지: 고객센터 - 1:1문의 목록
                qna_view.htm                -- 페이지: 고객센터 - 1:1문의 뷰
                qna_write.htm               -- 페이지: 고객센터 - 1:1문의 쓰기
                refund_list.htm             -- 페이지: 환불조회 목록
                refund_stp1.htm             -- 페이지: 환불신청 스텝1
                refund_stp2.htm             -- 페이지: 환불신청 스텝2
                refund_view.htm             -- 페이지: 환불조회 뷰
            css/                            -- 리소스 : CSS () /style.php?imgUrl=이미지도메인&skin=스킨명&css=불러올CSS[common|main|reset] )
                common.php                  -- CSS 공통
                main.php                    -- CSS 메인
                reset.php                   -- CSS reset
            data/                           -- 
            error/                          -- 페이지: 에러페이지
                dbError.htm                 -- 페이지: DB 에러
                expPage.htm                 -- 페이지: 잘못된 경로 접근
                notPage.htm                 -- 페이지: 없는 페이지
            event/                          -- 페이지: 이벤트 (rewrite 걸려있어서 /event/cosmoPass_1906 같은 형식으로 URL)
                {이벤트이름}.htm             -- 페이지: 이벤트 (URL /event/{이벤트이름})
                                            -- 페이지: 이벤트 (DB에서 조회) /event/?evt_no={이벤트넘버}
                cross_evt_exec.php          -- 페이지: 사이크 크로스 이벤트 데이터 처리
                cross_step_evt_exec.php     -- 페이지: 사이크 크로스 이벤트 (스탭처리)
                cross_step_evt.htm          -- 페이지: 사이트 크로스 이벤트 뷰
            evtpop/                         -- 
                reviewPop_1812.htm          -- 페이지: 이벤트팝업으로 추정(근데 현재는 사용하지 않는 것으로 보임)
            fvod/                           -- 페이지: 무료강의
                explain_info.htm            -- 페이지: 해설강의
                free_chr.htm                -- 페이지: 무료강의 - 커리큘럼
                fvod_rpy.htm                -- 페이지: 무료강의 - 댓글 뷰
                lct_list.htm                -- 페이지: 무료강의 - 강의 목록 (lct_list.php?fvod_cd=$fvod)
                tcc_list.htm                -- 페이지: 무료강의 - 가로세로캐스트
                tcc_view.htm                -- 페이지: 무료강의 - 가로세로캐스트 뷰
            inc/                            -- 
                footer.htm                  -- 
                header.htm                  -- 
                pop_footer.htm              -- 
                pop_header.htm              -- 
                pop_test.htm                -- 
                sky_left.htm                -- 
                sky_right.htm               -- 
                smenu.htm                   -- 
            lab/                            -- 페이지: 연구소
                guideline.htm               -- 페이지: 연구소 소개
                std_analy1.htm              -- 페이지: 연구소(덩허접수능영어) - 수능시행개요 - https://www.dhjengsn.com/lab/std_analy.php?sdx=1
                std_analy2.htm              -- 페이지: 연구소(덩허접수능영어) - 수능영어출제경향 - https://www.dhjengsn.com/lab/std_analy.php?sdx=2
                std_analy3.htm              -- 페이지: 연구소(덩허접수능영어) - 절대평가개요 - https://www.dhjengsn.com/lab/std_analy.php?sdx=3
                std_info1.htm               -- 페이지: 연구소(덩허접수능영어) - EBS&적중믿지마라 - https://www.dhjengsn.com/lab/std_info.php?sdx=1
                std_info2.htm               -- 페이지: 연구소(덩허접수능영어) - 선해석후독해 - https://www.dhjengsn.com/lab/std_info.php?sdx=2
            mail/                           -- 
                140421.htm                  -- 메일폼: 사이트 합병 메일
                birthday.htm                -- 메일폼: 생일 축하 메일
                email_auth.htm              -- 메일폼: 이메일인증 메일
                join.htm                    -- 메일: 가입축하메일
                mail_relax_after.htm        -- 메일폼: 휴면처리완료메일
                mail_relax.htm              -- 메일폼: 휴면안내메일
                pay.htm                     -- 메일폼: 결제완료메일
                search_pw.htm               -- 메일폼: 비밀번호 찾기 메일
                sso.htm                     -- 메일폼: 통합회원 안내 메일
                withdraw.htm                -- 메일폼: 탈퇴메일
            member/                         -- 
                id_search.htm               -- 페이지: 아이디 찾기
                idnpw_search.htm            -- 페이지: 비밀번호 찾기
                info_search.htm             -- 페이지: 아이디/비밀번호 찾기 인트로
                join_band_add2paper.htm     -- 페이지: 회원가입(옛날 것으로 추정)(레이어, iframe으로 열릴 것으로 추정)
                join.htm                    -- 페이지: 회원가입 스탭1
                join_step1.htm              -- 페이지: 회원가입 약관 동의 및 이름, 핸드폰 인증
                join_step2.htm              -- 페이지: 회원가입 정보입력
                join_step3.htm              -- 페이지: 회원가입완료
                join_stp1.htm               -- 페이지: 회원가입(옛날 것으로 추정) 스탭1
                join_stp2.htm               -- 페이지: 회원가입(옛날 것으로 추정, 20190905까지도 쓰고 있음) 완료 페이지
                join_stp2_test.htm          -- 페이지: 회원가입(옛날 것으로 추정) 스탭2(일부만)
                login.htm                   -- 페이지: 로그인(레이어, iframe으로 열림)
                login_rst.htm               -- 페이지: 로그인 완료(레이어, iframe으로 열림)
                mdf_info.htm                -- 페이지: 회원정보관리/비밀번호관리/회원탈퇴
                pw_search.htm               -- 페이지: 비밀번호 찾기
                sso.htm                     -- 
            mypage/                         -- 페이지: 마이페이지
                bank.htm                    -- 페이지: 마이페이지 - 캐시/마일리지/쿠폰/체험권
                basket.htm                  -- 페이지: 마이페이지 - 장바구니
                board_adminview.htm         -- 페이지: 마이페이지 - 학습Q&A 답변 뷰
                board.htm                   -- 페이지: 마이페이지 - 학습Q&A 목록
                board_message.htm           -- 페이지: 받은 쪽지함
                board_message_save.htm      -- 페이지: 쪽지 보관함
                board_message_view.htm      -- 페이지: 쪽지 뷰
                board_std_edit.htm          -- 페이지: 마이페이지 - 학습Q&A 수정
                board_std_view.htm          -- 페이지: 마이페이지 - 학습Q&A 뷰
                board_std_write.htm         -- 페이지: 마이페이지 - 학습Q&A 글쓰기
                index.htm                   -- 페이지: 마이페이지 - 메인
                ox_test.htm                 -- 페이지: OX테스트(안쓰는 페이지 같음)
                pay_list.htm                -- 페이지: 주문/배송조회 목록
                pay_view.htm                -- 페이지: 주문/배송조회 뷰
                std_list.htm                -- 페이지: 수강중인강좌/수강종료강좌 목록
                std_view.htm                -- 페이지: 수강중인강좌/수강종료강좌 뷰
                word_test.htm               -- 페이지: 단어테스트(덩허접수능영어, 덩허접기초, 덩허접공무원영어스쿨)(/bridge.php?path=/mypage/word_test.htm)
            payment/                        -- 페이지: 수강신청(결제)
                index.htm                   -- 페이지: 수강신청(결제)
                result.htm                  -- 페이지: 수강신청 완료(주문내역확인)
            player/                         -- 
                expn_player.htm             -- 
                flash_player.htm            -- 플레이어: 무료 강좌 플레이어(iframe, /player/flash_player.php?fvod_cd=56&w=730&h=411&auto=0)
                player.htm                  -- 플레이어: 강좌 플레이어
                player_kollus2.htm          -- 
                player_kollus.htm           -- 
                player_preview_right.htm    -- 
                player_std_right.htm        -- 
            popup/                          -- 페이지: 팝업
                exam/                       -- 페이지: 온라인모의고사
                    exam_pdf_down.php       -- 페이지: 온라인모의고사 파일 다운로드
                    exam_step1.htm          -- 페이지: 온라인모의고사
                    exam_step2.htm          -- 페이지: 온라인모의고사
                    exam_step3.htm          -- 페이지: 온라인모의고사
                    exam_step4.htm          -- 페이지: 온라인모의고사
                mock/                       -- 페이지: 온라인모의고사
                    mock_ajax.php           -- 페이지: 온라인모의고사
                    mock_dtl.htm            -- 페이지: 온라인모의고사 결과보기/인쇄 (https://www.gasehisexam.com/popup/popup.php?path=/mock/mock_dtl.htm&rst_seq=500&mod=)
                    mock_enter.htm          -- 페이지: 온라인모의고사 인트로
                    mock_list.htm           -- 페이지: 온라인모의고사 시험이력 (https://www.gasehisexam.com/popup/popup.php?path=/mock/mock_list.htm)
                    mock_pdfdown.php        -- 페이지: 온라인모의고사 PDF 다운로드
                    mock_stare.htm          -- 페이지: 온라인모의고사 진행
                    mock_start.htm          -- 페이지: 온라인모의고사 시작 (https://www.gasehisexam.com/popup/popup.php?path=/mock/mock_start.htm)
                svr/                        -- 페이지: 설문조사
                    svr.htm                 -- 페이지: 설문조사
                cash_charge.htm             -- 페이지: 캐시충전
                cpn_reg2.htm                -- 페이지: 체험권 등록
                cpn_reg.htm                 -- 페이지: 쿠폰 등록
                cpn_use.htm                 -- 페이지: 쿠폰 사용
                device_init.htm             -- 페이지: 모바일기기초기화 신청하기
                personal_policy.htm         -- 페이지: 개인정보취급방침
                point_charge.htm            -- 페이지: 마일리지 충전(쿠폰 등록)
                pop_info.htm                -- 페이지: 팝업 목록 JSON ()
                pop_typ_1.htm               -- 페이지: 팝업 뷰
                popup_relax_my.htm          -- 페이지: 계정 휴면상태 알림 팝업
                search_address.htm          -- 페이지: 주소검색
                service_policy.htm          -- 페이지: 이용약관
                simple_test_exe.htm         -- 페이지: 셀프테스트 관련
                simple_test_exe.php         -- 페이지: 셀프테스트 관련
                simple_test_his.htm         -- 페이지: 셀프테스트 관련
                simple_test.htm             -- 페이지: 셀프테스트 관련
                simple_test_list.htm        -- 페이지: 셀프테스트 관련
                simple_test_print.htm       -- 페이지: 셀프테스트 관련
                simple_test_rst.htm         -- 페이지: 셀프테스트 관련
                simple_test_sel.htm         -- 페이지: 셀프테스트 관련 (https://www.dhjengsn.com/popup/popup.php?path=simple_test_sel.htm)
                std_ext.htm                 -- 페이지: 수강연장하기
                std_noti.htm                -- 페이지: 학습 안내사항
                std_proof.htm               -- 페이지: 수강증발급
                std_stp.htm                 -- 페이지: 일시정지하기
                today_eval.htm              -- 페이지: 오늘의 학습 후기 작성
                simple_test_sel.htm         -- 
            prod/                           -- 페이지: 상품소개 및 판매
                chr_info.htm                -- 페이지: 커리큘럼
                chr_list.htm                -- 페이지: 커리큘럼 목록
                chr_view.htm                -- 페이지: 커리큘럼 뷰
                pass_view.htm               -- 페이지: 패스뷰
                pkg_view.htm                -- 페이지: 패키지뷰
            promotion/                      -- 페이지: 프로모션
            tch/                            -- 페이지: 선생님
                chr_intro{idx}_{tab}.htm    -- 페이지: 선생님 인트로 /tch/chr_intro.php?idx=1&tab=6
                chr_intro.htm               -- 페이지: 선생님 인트로 페이지 (이 페이지에서 chr_intro{idx}_{tab}.htm 로드)
                site_info{idx}.htm          -- 페이지: 사이트소개
                tch_info.htm                -- 페이지: 선생님 소개 (이 페이지에서 tch_info_{idx}.htm 로드)
            config_set.inc                  -- 설정파일: 사이트별 정보(결제정보, 사이트정보, 기타정보, 게시판정보 등)
            error.htm                       -- 페이지: 에러
            main.htm                        -- 페이지: 메인
            menu_set.inc                    -- 설정파일: 메뉴정보
    tch/                                    -- 페이지: 선생님
        chr_intro.php                       -- 페이지: 선생님 인트로 /tch/chr_intro.php?idx=1&tab=6
        dhj_intro.php                       -- 페이지: 선생님 인트로(덩허접 전용인 것으로 추정)
        site_info.php                       -- 페이지: 사이트소개
        tch_info.php                        -- 페이지: 선생님 소개 (이 페이지에서 tch_info_{idx}.htm
    test/                                   -- 테스트 파일 모음집?
    ad_set.php                              -- ???
    BraveCompany.plist                      -- iOS 인하우스 버전 패키지를 위한
    bridge.php                              -- 특별한 추가 데이터 불러오는 것 없이 정적인 페이지를 호출 할 때 사용 (예: bridge.php?path=/board/guide.htm)
    DataList.php                            -- 어디에 쓰는지 모르겠지만, 닥터보카, 덩허접기초에서 사용하는걸로 보임
    gabia.php                               -- PHP Info 출력
    index.php                               -- 사이트 인덱스
    install.html                            -- iOS 앱 인스톨을 위한 페이지 (안쓰는 것 같음)
    pop_test.php                            -- 페이지: 팝업 테스트??
    robots.txt                              -- 페이지: robots.txt 정책
    siteClose.htm                           -- 페이지: 서비스 종료 안내
    sso_test.php                            -- SSO 로그인 세션 확인 페이지
    style.php                               -- 페이지: imgUrl 설정, 공통 CSS, 각 사이트 CSS 로드
    syndication.php                         -- 네이버 syndication 테스트용으로 썼던 것 같음 (덩허접영어스쿨)
    {스킨명}_sitemap.xml                     -- 각 사이트별 사이트맵 XML
    world56n.htm                            -- 덩허접공무원영어 www.rankeyup.com 사이트에 특정 정보 보낼려고 썼던 것 같음
    world56n_start.htm                      -- 덩허접공무원영어 www.rankeyup.com 사이트에 특정 정보 보낼려고 썼던 것 같음